<?php

class BaseController extends Controller {

	protected $perPage = 12;

	protected $options;

	public function __construct()
	{
		// Get default options from cache or database
		$options = Option::get_options();

		// Share the options with other controllers.
		$this->options = $options;

		// Setting some default configurations for the script
		Config::set('application.url', $options->url);

		// Share the variables with views
		View::share('options', $options);

		// $genreCloud = Genre::all();
		// View::share('genreCloud', $genreCloud);

		// $favouriteShows = FavouriteShow::with('show')->groupBy('show_id')->orderBy('id', 'DESC')->limit(10)->get();
		// View::share('favouriteShows', $favouriteShows);

	}


	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}