<?php

class Option extends Eloquent {

    protected static $unguarded = true;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;



    public static function get_options()
    {
        $data = Option::all();
        $options = (object) array( 'raw'=> (object) array() );

        foreach ($data as $row)
        {
            $temp = $row->name;
            $options->{$temp} = $row->original['value'];
            $options->raw->{$temp} = (object) $row->original;
        }
        return $options;
    }

}