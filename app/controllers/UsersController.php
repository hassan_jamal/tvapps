<?php

class UsersController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /users
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user()->load(array('episodeLinkRequests.episode.show', 'episodeLinks.episode.show', 'favourites.episode.show', 'watchlist.episode.show', 'favouriteShows.show', 'watchlistShow.show'));

        return View::make('users.dashboard')->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     * GET /users/create
     *
     * @return Response
     */
    public function create()
    {
        return View::make('users.register');
    }

    /**
     * Store a newly created resource in storage.
     * POST /users
     *
     * @return Response
     */
    public function store()
    {
        if (!Sentry::check()) {
            $credentials = [
                'first_name' => Input::get('first_name'),
                'last_name'  => Input::get('last_name'),
                'email'      => Input::get('email'),
                'password'   => Input::get('password'),
                'activate'   => 'true',
            ];

            Sentry::create($credentials);
            return Redirect::to('/')->with('message', 'User has been Register');
        } else {
            return Redirect::to('/')->with('message', 'User is already logged in');
        }
    }

    public function login()
    {
        return View::make('users.login');
    }

    public function loginCheck()
    {
        $credentials = [
            'email'    => Input::get('email'),
            'password' => Input::get('password'),
        ];

        $user = Sentry::findByCredentials($credentials);

        if (Sentry::authenticate($credentials)) {
            return Redirect::intended();
        } else {
            return Redirect::back()->with('message', 'Your username/password combination was incorrect')->withInput();
        }
    }


    public function logout()
    {
        Sentry::logout();
        return Redirect::intended();
    }


    /**
     * Display the specified resource.
     * GET /users/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /users/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit()
    {
        return View::make('dashboard');
    }

    /**
     * Update the specified resource in storage.
     * PUT /users/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update()
    {
    //

    }

    /**
     * Remove the specified resource from storage.
     * DELETE /users/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}