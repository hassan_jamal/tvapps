<?php
// TODO
// Change related shows to other episodes of same seson in showEpisode

// Check for already rated in addRating

class ShowController extends \BaseController {

	/**
	 * Display latest added shows
	 * GET /
	 *
	 * @return Response
	 */
	public function index()
	{
		$shows = Show::orderBy('id', 'DESC')->paginate( $this->perPage );

		return View::make('shows.shows')->with('shows', $shows);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function search()
	{
		$shows = Show::where( 'name', 'LIKE', '%'.Input::get('search').'%')->paginate( $this->perPage );

		return View::make('shows.search')
					->with('shows', $shows )
					->with('search', Input::get('search'));
	}

	/**
	 * Display latest added shows for specific genre
	 * GET /genre/{id}-{slug}
	 *
	 * @param  int  $id
	 * @param  string  $slug
	 * @return Response
	 */
	public function indexGenre( $id, $slug )
	{
		$genre = Genre::with('shows')->find($id);

		$shows = $genre->shows()->orderBy('id', 'DESC')->paginate( $this->perPage );

		return View::make('shows.genre')
					->with('genre', $genre)
					->with('shows', $shows);
	}


	/**
	 * Display latest added shows for specific year
	 * GET /year/{id}-{slug}
	 *
	 * @param  int  $year
	 * @return Response
	 */
	public function indexYear( $year )
	{
		$shows = Show::where( 'start_date', 'LIKE', "%".$year )
					->orderBy('id', 'DESC')
					->paginate( $this->perPage );

		return View::make('shows.year')
					->with('year', $year)
					->with('shows', $shows);
	}


	/**
	 * Display link requests
	 * GET /requests
	 *
	 * @return Response
	 */
	public function indexRequests( )
	{
		$linkRequests = EpisodeLinkRequest::with('episode.show')->groupBy('episode_id')
					->get( array('episode_id',DB::raw('COUNT(*) as `count`')) );

		// return $linkRequests;
		return View::make('shows.requests')->with('linkRequests', $linkRequests);
	}


	/**
	 * Show the form for creating a new resource.
	 * GET /show/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /show
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display specific show details
	 * GET /show/{id}-{slug}
	 *
	 * @param  int  $id
	 * @param  string  $slug
	 * @return Response
	 */
	public function show($id, $slug)
	{
		$show = Show::with( array('sessions'=> function($q)
        {
        	$q->orderBy('id', 'DESC');
            $q->with( array('episodes'=> function($q)
	        {
	        	$q->orderBy('id', 'DESC');
	            $q->with('links');
	        }));
        }, 'genre','rating') )->find($id);

		// return $show;
		// Fix the date format
		$show->started = substr($show->start_date, -4);

		// Calculate rating
		$rating = calculateRaiting( $show->rating );

		// Find related shows
		$shows = Show::whereClass( $show->class )->where('id', '!=', $show->id )->limit(4)->get();

		return View::make('shows.show')
				->with('shows', $shows)
				->with('show', $show)
				->with('rating', $rating);
	}




	/**
	 * Show the form for editing the specified resource.
	 * GET /show/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /show/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /show/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	/**
	 * Add a show to watchlist
	 * GET /watchlist/add/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function addToWatchlistShow( $id )
	{
		WatchlistShow::firstOrCreate(array(
			'show_id'	=> $id,
			'user_id'	=>	Auth::user()->id
		));
		return Redirect::back()->with('message', trans('show.watchlist.added') );
	}


	/**
	 * Delete a show from watchlist
	 * GET /watchlist/add/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroyWatchlistShow( $id )
	{
		WatchlistShow::find( $id );
		return Redirect::back()->with('message', trans('show.watchlist.deleted') );
	}


	/**
	 * Add a show to watched list
	 * GET /watched/add/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function addToWatchedShow( $id )
	{
		WatchedShow::firstOrCreate(array(
			'show_id'	=> $id,
			'user_id'	=>	Auth::user()->id
		));
		return Redirect::back()->with('message', trans('show.watchedlist.added') );
	}


	/**
	 * Add a show to favourite list
	 * GET /favorites/add/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function addToFavoriteShow( $id )
	{
		FavouriteShow::firstOrCreate(array(
			'show_id'	=> $id,
			'user_id'	=>	Auth::user()->id
		));
		return Redirect::back()->with('message', trans('show.favourite.added') );
	}

	/**
	 * Delete a show from favourite list
	 * GET /favorites/delete/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroyFavoriteShow( $id )
	{
		FavouriteShow::find($id)->delete();
		return Redirect::back()->with('message', trans('show.favourite.deleted') );
	}


	/**
	 * Add rating for a show
	 * GET /show/rating/{showId}-{rating}
	 *
	 * @param  int  $showId
	 * @param  int  $rating
	 * @return Response
	 */
	public function addShowRating( $showId, $rating )
	{
		$showRating = ShowRating::firstOrNew(array(
			'show_id'	=> $showId,
			'user_id'		=>	Auth::user()->id
		));

		if( !isset($showRating->rating) )
		{
			$showRating->rating = $rating;
			$showRating->save();
			$msg = trans('master.rating.added');
		}
		else
		{
			$showRating->rating = $rating;
			$showRating->update();
			$msg = trans('master.rating.updated');
		}
		return Redirect::back()->with('message', $msg);
	}





}