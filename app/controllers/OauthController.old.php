<?php


class OauthController extends \BaseController
{

    /**
     * getFacebookAuthorize
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getFacebookAuthorize()
    {
        // get data from input
        $code = Input::get('code');

        // get fb service
        $fb = OAuth::consumer('Facebook');

        // check if code is valid

        // if code is provided get user data and sign in
        if (!empty($code)) {

            // This was a callback request from facebook, get the token
            $token = $fb->requestAccessToken($code);

            // Send a request with it
            $result = json_decode($fb->request('/me'), true);

//            $message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
//            echo $message . "<br/>";

            try {

                $user = Sentry::findUserByCredentials([
                    'email' => $result['email'],
                ]);

                Sentry::login($user, false);

                return Redirect::to('/login')->with('message', 'You have been logged in using facebook');
            } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
                Sentry::createUser([
                    'email'      => $result['email'],
                    'first_name' => $result['first_name'],
                    'last_name'  => $result['last_name'],
                    'password'   => $result['name'],
                    'social_id'  => $result['id'] ,
                ]);

                $user = Sentry::findUserByCredentials([
                    'email' => $result['email'],
                ]);

                $user->activated = 'true';
                $user->save();

                Sentry::login($user, false);

                return Redirect::to('/login')->with('message', 'your new id has been created and You have been logged in using facebook ');
            }
        } // if not ask for permission first
        else {
            // get fb authorization
            $url = $fb->getAuthorizationUri();

            // return to facebook login url
            return Redirect::to((string)$url);
        }
    }

    public function getGoogleAuthorize()
    {
        // get data from input
        $code = Input::get('code');

        // get google service
        $googleService = OAuth::consumer('Google');

        // check if code is valid

        // if code is provided get user data and sign in
        if (!empty($code)) {

            // This was a callback request from google, get the token
            $token = $googleService->requestAccessToken($code);

            // Send a request with it
            $result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);

            $message = 'Your unique Google user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
            echo $message . "<br/>";

            //Var_dump
            //display whole array().
            dd($result);

        } // if not ask for permission first
        else {
            // get googleService authorization
            $url = $googleService->getAuthorizationUri();

            // return to facebook login url
            return Redirect::to((string)$url);
        }
    }

    public function getTwitterAuthorize()
    {
        $oauth_token = Input::get('oauth_token');
        $oauth_verifier = Input::get('oauth_verifier');
        // get service
        $twit = OAuth::consumer('Twitter');

        // check if code is valid

        // if code is provided get user data and sign in
        if (!empty($oauth_token)) {

            // This was a callback request from google, get the token
            $token = $twit->requestAccessToken($oauth_token, $oauth_verifier);

            // Send a request with it
            $result = json_decode($twit->request('account/verify_credentials.json'), true);

            dd($result);

//            $user = Sentry::getUser()

        } // if not ask for permission first
        else {
            // get authorization
            $token = $twit->requestRequestToken();
            $url = $twit->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));

            // return to login url
            return Redirect::to((string)$url);
        }
    }

    public function getYahooAuthorize()
    {
        $oauth_token = Input::get('oauth_token');
        $oauth_verifier = Input::get('oauth_verifier');
        $yahoo = OAuth::consumer('Yahoo');

        if (!empty($oauth_token)) {
            $token = $yahoo->requestAccessToken($oauth_token, $oauth_verifier);
            $result = json_decode($yahoo->request('profile'), true);
            dd($result);
        } else {
            $token = $yahoo->requestRequestToken();
            $url = $yahoo->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));

            // return to login url
            return Redirect::to((string)$url);
        }
    }


}


