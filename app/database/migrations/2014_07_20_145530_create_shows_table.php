<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShowsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shows', function(Blueprint $table)
		{
						$table->increments('id');
            $table->integer('show_id');
            $table->integer('seasons');
            $table->string('name');
            $table->string('link');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('image');
            $table->string('origin');
            $table->string('status');

            $table->unsignedInteger('class');

            $table->integer('runtime');
            $table->string('network');
            $table->time('airtime');
            $table->string('airday');
			$table->timestamps();

            $table->unique('show_id');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shows');
	}

}
