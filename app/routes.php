<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'shows', 'uses' => 'ShowController@index'));
Route::get('/logout', array('as' => 'logout', 'uses' => 'UsersController@logout'));
Route::get('search', array('as' => 'search', 'uses' => 'ShowController@search'));
Route::get('requests', array('as' => 'requests', 'uses' => 'ShowController@indexRequests'));


Route::group(array('before' => 'isGuest'), function () {
    Route::get('register', array('as' => 'register', 'uses' => 'UsersController@create'));
    Route::post('register', array('before' => 'csrf', 'uses' => 'UsersController@store'));

    Route::get('login', array('as' => 'login', 'uses' => 'UsersController@login'));
    Route::post('login', array('before' => 'csrf', 'uses' => 'UsersController@loginCheck'));

    Route::get('oauth/authorize/{serviceName}', array('as' => 'authorize', 'uses' => 'OAuthController@getAuthorize'));
    Route::controller('oauth', 'OAuthController');

});





