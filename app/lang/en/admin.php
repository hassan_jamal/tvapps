<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "options"       =>  array(
        'name'          =>  'Site Name',
        'url'           =>  'Site Url',
        'adCode'        =>  'Ad Code',
        'copyright'     =>  'Copyright Text',
        'footerLinks'   =>  'Footer Links',
        'socialLinks'   =>  'Social Links',
    ),

    "help"       =>  array(
        'name'          =>  '',
        'url'           =>  'eg. http://domain.com',
        'adCode'        =>  'You can use HTML in here',
        'copyright'     =>  '',
        'footerLinks'   =>  'You can use HTML in here',
        'socialLinks'   =>  'You can use HTML in here',
    ),




);
