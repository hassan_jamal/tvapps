<?php

class Show extends \Eloquent {
	protected $fillable = [
	'show_id',
	'seasons',
	'name',
	'link',
	'start_date',
	'end_date',
	'image',
	'origin',
	'status',
	'class',
	'runtime',
	'airtime',
	'airday'
	];
	/**
	 * define table
	 * @var string
	 */
	protected $table    = 'shows';


}