@extends('master')

@section('title', " | " . $show->name . " | " . $episode->title)
@section('description', $episode->summary)

@section('content')
    <section>
        <h3>
            <a href="{{ route('show', array($show->id, Str::slug($show->name) ))}}">
                {{ $show->name }}
            </a>
        </h3>
        <h2>{{ $episode->code }} - {{ $episode->title }}</h2>
    </section>

    <br>
    <section class="show-info row">
        <div class="col-md-6">
            <div class="thumbnail">
                <img src="{{ route('thumbnail', array( 'episode',$show->id, $episode->id) ) }}" alt="{{ $episode->title }}" />
            </div>

            @include('parts.episodeRating')

            <div class="btn-group btn-group-justified">
                <!-- <a class="btn btn-primary" title="Remove from WatchList" href="#"><span class="fa fa-trash-o"></span></a> -->
                <a class="btn btn-primary showTooltip" title="Add to Favorite" href="{{ route('addToFavorite', $episode->id ) }}" data-toggle="tooltip" data-placement="bottom"><span class="fa fa-heart"></span></a>
                <a class="btn btn-primary showTooltip" title="Add to WatchList" href="{{ route('addToWatchlist', $episode->id ) }}" data-toggle="tooltip" data-placement="bottom"><span class="fa fa-plus"></span></a>
                <a class="btn btn-primary showTooltip" title="Already Watched" href="{{ route('addToWatchedEpisode', $episode->id ) }}" data-toggle="tooltip" data-placement="bottom"><span class="fa fa-check"></span></a>
            </div>
        </div>

        <div class="col-md-6">
            @if( !is_null($episode->summary) )
                <p>{{ $episode->summary }}</p>
            @endif

            <table class="table table-condensed">
                <!-- <tr>
                    <td class="sideleft">Episode Number: </td>
                    <td class="sideleft">{{ $episode->number }}</td>
                </tr>
                <tr>
                    <td class="sideleft">Production Number: </td>
                    <td class="sideleft">{{ $episode->prod_num }}</td>
                </tr>
                <tr>
                    <td class="sideleft">Episode Title: </td>
                    <td class="sideleft">{{ $episode->title }}</td>
                </tr> -->
                <tr>
                    <td class="sideleft">Airs: </td>
                    <td class="sideleft">{{ $episode->air_date }}</td>
                </tr>
            </table>

            <hr>
            @include('parts.show_desc')


            <!-- <br> -->
            <!-- <div class="btn-group btn-group-justified">
                <a class="btn btn-small btn-primary" href="{{ route('addLink', $episode->id) }}">
                    <i class="fa fa-plus"></i> Add New Link
                </a>
                <a class="btn btn-small btn-warning" href="{{ route('requestLink', $episode->id) }}">
                    Request Links
                </a>
            </div>
            <br> -->
            <a href="#" target="_blank" class="btn btn-info btn-block">Stream in HD <i class="fa fa-play fa fa-white"></i></a>
        </div>

    </section>

    <br><hr>

    <section class="user-buttons">
        <div class="clearfix">
            <h3 class="pull-left">Links for episode: {{ $episode->title }}</h3>
            <div class="pull-right" style="margin-top: 15px;">
                <a class="btn btn-sm btn-primary" href="{{ route('addLink', $episode->id) }}">
                    <i class="fa fa-plus"></i> Add New Link
                </a>
                <a class="btn btn-sm btn-warning" href="{{ route('requestLink', $episode->id) }}">
                    Request Links
                </a>
            </div>
        </div>
        <br>

        @if( isset($episode->links[0]) )
            <div class="movie-links">
                <table class="table table-hover table-striped sort-table" id="streamlinks">
                    <thead>
                        <tr>
                            <th>Link</th>
                            <th>Age</th>
                            <th>Vote</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach( $episode->links as $link )
                            <tr>
                                <td>
                                    <img src="http://www.google.com/s2/favicons?domain_url={{ $link->domain }}" alt="">
                                    &nbsp;
                                    <a href="{{ $link->link }}" target="_blank" rel="nofollow">
                                        {{ ucfirst( $link->domain ) }}
                                    </a>
                                </td>
                                <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($link->created_at))->diffForHumans() }}</td>
                                <td>
                                    <a href="{{ route('linkVote', array($link->id,'up') ) }}" class="btn btn-vote btn-sm up"><i class="fa fa-thumbs-up"></i> {{ countVotes( $link->votes, 1) }}</a>
                                    &nbsp;&nbsp;
                                    <a href="{{ route('linkVote', array($link->id, 'down') ) }}" class="btn btn-vote btn-sm down"><i class="fa fa-thumbs-down"></i> {{ countVotes( $link->votes, 0) }}</a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        @endif
    </section>

    <hr>
    @include('parts.comment', array('comment_url'=> route('episode', array($show->id, Str::slug($show->name), $episode->id, Str::slug($episode->title) ))  ))
    <hr>
    @include('parts.related')


@stop

