@extends('master')

@section('content')

    <section>
        <h3>Latest TV Shows</h3>
        <hr>

        @include('parts.shows')

    </section>

@stop

