@extends('master')

@section('content')

    <section>
        <h3>Latest TV Shows in genre: {{ $genre->name }}</h3>
        <hr>

        @include('parts.shows')

    </section>

@stop

