@extends('master')

@section('content')

    <section>

        <div class="row">
            <div class="col-md-7">

                <h3>Add a new link for episode: <a href="{{ route('episode', array( $episode->show->id, Str::slug($episode->show->name), $episode->id, Str::slug($episode->title) )) }}">{{ $episode->title }}</a></h3>

                <br><br>

                <form action="{{ route('addLink', $episode->id) }}" method="post" class="form-horizontal " role="form">

                    {{ Form::token() }}

                    <div class="input-group">
                        <input type="text" class="form-control" name="link" placeholder="http://domain.com/episode-name.html" required>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Add Link</button>
                        </span>
                    </div><!-- /input-group -->

                    <!-- <div class="form-group">
                        <label for="link" class="col-sm-2 control-label">Link</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="link" placeholder="http://domain.com/episode-name.html" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lang" class="col-sm-2 control-label">Language</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="lang" placeholder="English" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="quality" class="col-sm-2 control-label">Quality</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="quality" placeholder="HDTV" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Add Link</button>
                        </div>
                    </div> -->
                </form>

            </div>
            <div class="col-md-5">
                <br>
                {{ $options->adCode }}
            </div>
        </div>

    </section>

@stop

