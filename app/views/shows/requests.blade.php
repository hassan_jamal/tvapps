@extends('master')

@section('content')

    <section>
        <h3>Link Requests</h3>
        <hr>

        <table class="table table-hover table-striped">
            <tbody>
                @foreach( $linkRequests as $link )
                    <tr>
                        <td>
                            <a href="{{ route('episode', array( $link->episode->show->id, Str::slug($link->episode->show->name), $link->episode->id, Str::slug($link->episode->title) )) }}">
                                {{ $link->episode->title }}
                            </a>
                        </td>
                        <td>
                            <div class="btn-group pull-right">
                                <span href="" class="btn btn-sm btn-info">{{ $link->count }} Link Requests</span>
                                <a href="{{ route('addLink', $link->episode->id) }}" class="btn btn-sm btn-danger">Add Link</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </section>

@stop

