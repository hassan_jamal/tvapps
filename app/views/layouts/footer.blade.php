
        </div>
        <div class="col-md-4 sidebar">
            <div class="widget">
                {{ $options->adSidebar }}
            </div>
            <div class="widget most-popular">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr><th colspan="2"><h3>Most Popular Shows</h3></th></tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            <div class="widget">
                {{ $options->adSidebarBottom }}
            </div>

            </div>

        </div>

        <div class="clearfix"></div>

        <div class="row">
            <hr class="col-md-8 col-md-offset-2">
            <div class="col-md-12 genreCloud">
                <h3>Genres</h3>
                <br>

               
            </div>
            <hr class="col-md-8 col-md-offset-2">
            <div class="col-md-12 text-center">
                {{ $options->adFooter }}
            </div>
        </div>
    </div>



    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="pagination pull-left">
                    {{ $options->footerLinks }}
                    <p class="copyright">{{ $options->copyright }}</p>
                </div>

                <div class="pull-right">
                    <div class="pagination">
                        {{ $options->socialLinks }}
                        <a href="#" class="pull-right">To Top</a>
                    </div>
                </div>

            </div>

        </div>
    </footer> <!-- /container -->

    <script type="text/javascript">
        // $("[data-rel=tooltip]").tooltip();
        $(document).ready(function(){
            $(".fb-comments").attr("data-width", $(".fb-comments").parent().width());
        });
        $('.showTooltip').tooltip({container: 'body'});
    </script>



    </body>
</html>
