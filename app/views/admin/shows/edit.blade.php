@extends('admin.layout')

@section('content')

<style type="text/css" media="screen">

ul li .thumbnail>a>img{
    min-height: 200px;
    max-height: 200px;
    max-width: 162px;
}

</style>
    <section class="row">
        <form class="form-horizontal">
        <div class="col-md-8">
            <h3>Edit Show: {{ $show->name }}</h3>
            <br>
            <div class="form-group">
                <label for="name" class="col-md-3 control-label">Show Name</label>
                <div class="col-md-8">
                    <input type="text" name="name" id="name" class="form-control" value="{{ $show->name }}">
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="col-md-3 control-label">Summary</label>
                <div class="col-md-8">
                    <textarea name="name" id="name" class="form-control">{{ $show->summary }}</textarea>
                </div>
            </div>
        </div>

        <img class="thumbnail col-md-3 col-md-offset-1" src="{{ route('thumbnail', array( 'show',$show->id, 0) ) }}" alt="{{ $show->name }}">
        </form>
    </section>

@stop


