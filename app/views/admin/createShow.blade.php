@extends('admin.layout')

@section('content')

<style type="text/css" media="screen">
.center-block {
    float: none;
    margin-left: auto;
    margin-right: auto;
}

.input-group .icon-addon .form-control {
    border-radius: 0;
}

.icon-addon {
    position: relative;
    color: #555;
    display: block;
}

.icon-addon:after,
.icon-addon:before {
    display: table;
    content: " ";
}

.icon-addon:after {
    clear: both;
}

.icon-addon .fa {
    position: absolute;
    z-index: 2;
    right: 10px;
    font-size: 14px;
    width: 20px;
    margin-left: -2.5px;
    text-align: center;
    padding: 10px 0;
    top: 1px
}


.icon-addon.addon-sm .form-control {
    height: 30px;
    padding: 5px 28px 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    min-width: 320px;
}

.icon-addon.addon-sm .fa,
.icon-addon.addon-sm .fa {
    margin-left: 0;
    font-size: 12px;
    right: 5px;
    top: -1px
}

.icon-addon .form-control:focus + .fa,
.icon-addon:hover .fa,
.icon-addon .form-control:focus + .fa,
.icon-addon:hover .fa {
    color: #2580db;
}
</style>




        <div class="clearfix">
            <h3 class="pull-left" style="margin-top:0px">
                Add New Shows
            </h3>
        </div>

        <form action="{{ route('adminShowSearchNew') }}" method="get" id="searchForm" accept-charset="utf-8">
            <div class="form-group">
                <div class="icon-addon">
                    <input type="text" placeholder="Search Shows" id="search" name="search" class="form-control"
                    {{ Input::has('search') ? 'value="' . Input::get('search') . '"' : '' }}>
                    <label for="search" class="fa fa-search" rel="tooltip" title="search"></label>
                </div>
            </div>
        </form>


        <div class="list-group" id="result">



        </div>


    <script type="text/javascript">

        var jsonUrl = "{{ route('adminShowSearchNew') }}";

        $("#searchForm").submit(function(event){

            event.preventDefault();


            var search = $("#search").val();
            if (search.length == 0) {
                $("#search").focus();
            } else {
                $("#result").empty().html( "<div class='text-center' style='margin-top:60px;'><img src='{{ asset('assets/img/loading.gif') }}' alt='loading'></div>" );
                $.getJSON(
                    jsonUrl,
                    {search: search},
                    function(data) {
                        var items = [];
                        console.log(data);
                        $.each( data.show, function( key, show ) {
                            items.push( "<div href='#' class='list-group-item clearfix'><div class='pull-left'>" + show.showid + "  - " + show.name + " ( " + show.country + " )</div><div class='pull-right'><a href='{{ route('adminShowSearch', "") }}/" + show.showid + "' class='label label-success add'> <i class='fa fa-check-square-o'></i> Add </a><a href='" + show.link + "' class='label label-primary' target='_blank'> <i class='fa fa-eye'></i> View </a></div></div>");
                        });
                        // var result = "Language code is \"<strong>" + json + "\"";
                        $("#result").empty().html( items.join( "" ) );
                    }
                );
            }
            return false;
        });


        // $('#add').click( store() );
        // function store () {
            $("a.add").click(function( e ){
                e.preventDefault();
                e.stopPropagation();

                var jsonUrl = this.attr('href').val();

                $.getJSON(
                        jsonUrl,
                        function(data) {
                            var items = [];
                            alert( data );
                            // var result = "Language code is \"<strong>" + json + "\"";
                            // $("#result").html( items.join( "" ) );
                        }
                    );
                return false;
            });
        // }
    </script>




@stop