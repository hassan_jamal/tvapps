@extends('admin.layout')

@section('content')

<style type="text/css" media="screen">
.center-block {
    float: none;
    margin-left: auto;
    margin-right: auto;
}

ul li .thumbnail>a>img{
    min-height: 200px;
    max-height: 200px;
    max-width: 162px;
}

.input-group .icon-addon .form-control {
    border-radius: 0;
}

.icon-addon {
    position: relative;
    color: #555;
    display: block;
}

.icon-addon:after,
.icon-addon:before {
    display: table;
    content: " ";
}

.icon-addon:after {
    clear: both;
}

.icon-addon .fa {
    position: absolute;
    z-index: 2;
    right: 10px;
    font-size: 14px;
    width: 20px;
    margin-left: -2.5px;
    text-align: center;
    padding: 10px 0;
    top: 1px
}


.icon-addon.addon-sm .form-control {
    height: 30px;
    padding: 5px 28px 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    min-width: 320px;
}

.icon-addon.addon-sm .fa,
.icon-addon.addon-sm .fa {
    margin-left: 0;
    font-size: 12px;
    right: 5px;
    top: -1px
}

.icon-addon .form-control:focus + .fa,
.icon-addon:hover .fa,
.icon-addon .form-control:focus + .fa,
.icon-addon:hover .fa {
    color: #2580db;
}
</style>




        <div class="clearfix">
            <h3 class="pull-left" style="margin-top:0px">
                Manage Shows

            </h3>
            <form action="{{ route('adminShowSearch') }}" method="get" accept-charset="utf-8">

                <div class="form-group pull-right">
                    <div class="icon-addon addon-sm">
                        <input type="text" placeholder="Search Shows" name="search" class="form-control"
                        {{ Input::has('search') ? 'value="' . Input::get('search') . '"' : '' }}>
                        <label for="search" class="fa fa-search" rel="tooltip" title="search"></label>
                    </div>
                </div>
                <div class="pull-right">&nbsp;&nbsp;&nbsp;</div>
                <a href="{{ route('adminShowCreate') }}" class="btn btn-sm btn-success pull-right">Add New</a>
            </form>
        </div>


        <ul class="list-inline">
            @foreach( $shows as $show)
                <li class="">
                    <div class="thumbnail">
                        <a href="{{ route('adminShowEdit', $show->id) }}" title="{{ $show->name }}">
                            <img class="thumbimg" src="{{ route('thumbnail', array( 'show',$show->id, 0) ) }}" alt="{{ $show->name }}">
                        </a>
                        <div class="caption">
                            <a href="{{ route('adminShowEdit', $show->id) }}" title="{{ $show->name }}">{{ Str::limit($show->name, 16) }}</a><br>
                            <span class="">{{ Str::limit( \Carbon\Carbon::createFromTimeStamp(strtotime($show->created_at))->diffForHumans(), 12) }}</span>
                            <div class="pull-right">
                                <a href="{{ route('show', array( $show->id, Str::slug($show->name) ) ) }}"> <i class="fa fa-eye"></i>&nbsp; </a>
                                <a href="{{ route('adminShowDelete', $show->id) }}"> <i class="fa fa-trash-o"></i> </a>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>




        <div class="text-center">{{ $shows->appends(array('search' => Input::get('search') ))->links() }}</div>


@stop


