<div class="text-center rating">
    <input id="input-id" type="number" value="{{ $rating }}">
</div>

<br>

<script type="text/javascript">
    $("#input-id").rating(
        {
            'min':0,
            'max':5,
            'step':"0.5",
            'size':'xs',
            'showClear':false,
        }
    );



    $("#input-id").on("rating.change", function(event, value, caption) {

        window.location.replace("{{ route('episodeRating', $episode->id ) }}" + value);

        // $.get( "{{ route('episodeRating', $show->id ) }}", function( data ) {
        //         // $( ".result" ).html( data );
        //         alert( "Load was performed. " + data );
        // });
        // alert("You rated: " + value + " = " + $(caption).text());
    });
</script>