<section class="show-sec-last comments">
    <h3>Comments</h3>

    @if( Auth::check() )
        <div id="comments">
            <div class="fb-comments" data-href="{{ $comment_url }}" data-numposts="5" data-colorscheme="light"></div>
        </div>
    @else
        <h4 class="text-center">Please <a href="{{ route('register') }}"> register</a> or <a href="{{ route('login') }}">login</a> to comment.</h4>
    @endif

</section>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=248097288723025&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>