<section>
    <h3>Related TV Shows</h3>
    <ul class="thumbnails list-inline clearfix">
        @foreach( $shows as $show )
            @include('parts.show')
        @endforeach
    </ul>
</section>